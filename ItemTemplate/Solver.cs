﻿using AdventOfCode.Framework;
using System.Collections.Generic;

namespace $rootnamespace$.Day$fileinputname$
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "";

        public string SecondExpected => "";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                   ""
                }, "")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    ""
                }, "")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return "Nope";
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return "Nope";
        }
    }
}
