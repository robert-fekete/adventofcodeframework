﻿using System.Collections.Generic;

namespace AdventOfCode.Tools.OCR
{
    internal class Blob
    {
        private readonly bool[][] data;
        private readonly int first;
        private readonly int last;

        public Blob(bool[][] data, int first, int last)
        {
            this.data = data;
            this.first = first;
            this.last = last;
        }

        public bool IsEmpty => data[0].Length == 0;

        public int CalculateDataError(IEnumerable<IEnumerable<bool>> otherData)
        {
            var y = 0;
            var error = 0;
            foreach(var row in otherData)
            {
                var x = 0;
                foreach (var cell in row)
                {
                    if (data[y][x] != cell)
                    {
                        error++;
                    }
                    x++;
                }
                y++;
            }

            return error;
        }

        public bool IsSizeMatch(int length)
        {
            return length == data[0].Length;
        }

        public override string ToString()
        {
            return $"Blob: {first} -> {last}";
        }
    }
}