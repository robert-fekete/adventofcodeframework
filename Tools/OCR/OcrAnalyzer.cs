﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode.Tools.OCR
{
    public class OcrAnalyzer
    {
        public string ReadText(IEnumerable<IEnumerable<bool>> source)
        {
            var image = new Image(source);
            var blobs = image.GetBlobs();

            var builder = new StringBuilder();
            foreach (var blob in blobs)
            {
                var matchingChar = Characters.All.First();
                var minError = Characters.InitialError;
                foreach (var other in Characters.All)
                {
                    var error = other.CalculateError(blob);
                    if (minError > error)
                    {
                        matchingChar = other;
                        minError = error;
                    }
                }

                builder.Append(matchingChar.Name);
            }

            return builder.ToString();
        }

        private class Characters
        {
            public static int InitialError => int.MaxValue;
            public static Character[] All = new[]
            {
                new Character('A', '#', new char[][]
                {
                    new []{ ' ', '#', '#', ' '},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', '#', '#', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                }),

                new Character('B', '#', new char[][]
                {
                    new []{ '#', '#', '#', ' '},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', '#', '#', ' '},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', '#', '#', ' '},
                }),

                new Character('C', '#', new char[][]
                {
                    new []{ ' ', '#', '#', ' '},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ ' ', '#', '#', ' '},
                }),

                new Character('E', '#', new char[][]
                {
                    new []{ '#', '#', '#', '#'},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', '#', '#', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', '#', '#', '#'},
                }),

                new Character('F', '#', new char[][]
                {
                    new []{ '#', '#', '#', '#'},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', '#', '#', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', ' ', ' '},
                }),

                new Character('G', '#', new char[][]
                {
                    new []{ ' ', '#', '#', ' '},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', '#', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ ' ', '#', '#', '#'},
                }),

                new Character('H', '#', new char[][]
                {
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', '#', '#', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                }),

                new Character('I', '#', new char[][]
                {
                    new []{ '#', '#', '#' },
                    new []{ ' ', '#', ' ' },
                    new []{ ' ', '#', ' ' },
                    new []{ ' ', '#', ' ' },
                    new []{ ' ', '#', ' ' },
                    new []{ '#', '#', '#' },
                }),

                new Character('J', '#', new char[][]
                {
                    new []{ ' ', ' ', '#', '#'},
                    new []{ ' ', ' ', ' ', '#'},
                    new []{ ' ', ' ', ' ', '#'},
                    new []{ ' ', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ ' ', '#', '#', ' '},
                }),

                new Character('K', '#', new char[][]
                {
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', '#', ' '},
                    new []{ '#', '#', ' ', ' '},
                    new []{ '#', ' ', '#', ' '},
                    new []{ '#', ' ', '#', ' '},
                    new []{ '#', ' ', ' ', '#'},
                }),

                new Character('L', '#', new char[][]
                {
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', '#', '#', '#'},
                }),

                new Character('O', '#', new char[][]
                {
                    new []{ ' ', '#', '#', ' '},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ ' ', '#', '#', ' '},
                }),

                new Character('P', '#', new char[][]
                {
                    new []{ '#', '#', '#', ' '},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', '#', '#', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', ' ', ' '},
                }),

                new Character('R', '#', new char[][]
                {
                    new []{ '#', '#', '#', ' '},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', '#', '#', ' '},
                    new []{ '#', ' ', '#', ' '},
                    new []{ '#', ' ', ' ', '#'},
                }),

                new Character('S', '#', new char[][]
                {
                    new []{ ' ', '#', '#', '#'},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ ' ', '#', '#', ' '},
                    new []{ ' ', ' ', ' ', '#'},
                    new []{ '#', '#', '#', ' '},
                }),

                new Character('U', '#', new char[][]
                {
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', '#'},
                    new []{ ' ', '#', '#', ' '},
                }),

                new Character('Y', '#', new char[][]
                {
                    new []{ '#', ' ', ' ', ' ', '#'},
                    new []{ '#', ' ', ' ', ' ', '#'},
                    new []{ ' ', '#', ' ', '#', ' '},
                    new []{ ' ', ' ', '#', ' ', ' '},
                    new []{ ' ', ' ', '#', ' ', ' '},
                    new []{ ' ', ' ', '#', ' ', ' '},
                }),

                new Character('X', '#', new char[][]
                {
                    new []{ '#', ' ', ' ', ' ', '#'},
                    new []{ ' ', '#', ' ', '#', ' '},
                    new []{ ' ', ' ', '#', ' ', ' '},
                    new []{ ' ', '#', ' ', '#', ' '},
                    new []{ ' ', '#', ' ', '#', ' '},
                    new []{ '#', ' ', ' ', ' ', '#'},
                }),

                new Character('Z', '#', new char[][]
                {
                    new []{ '#', '#', '#', '#'},
                    new []{ ' ', ' ', ' ', '#'},
                    new []{ ' ', ' ', '#', ' '},
                    new []{ ' ', '#', ' ', ' '},
                    new []{ '#', ' ', ' ', ' '},
                    new []{ '#', '#', '#', '#'},
                }),

                //new Character('B', '#', new char[][]
                //{
                //}),
            };
        }
    }
}
