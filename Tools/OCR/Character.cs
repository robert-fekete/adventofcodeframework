﻿using System;
using System.Linq;

namespace AdventOfCode.Tools.OCR
{
    internal class Character
    {
        private readonly char blackPixel;
        private readonly char[][] data;

        public Character(char name, char blackPixel, char[][] data)
        {
            Name = name;
            this.blackPixel = blackPixel;
            this.data = data;
        }

        public char Name { get; }

        internal int CalculateError(Blob blob)
        {
            if (!blob.IsSizeMatch(data[0].Length))
            {
                return data[0].Length * data.Length;
            }

            return blob.CalculateDataError(data.Select(r => r.Select(c => c == blackPixel)));
        }
    }
}