﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Tools.OCR
{
    internal class Image
    {
        private bool[][] source;

        public Image(IEnumerable<IEnumerable<bool>> source)
        {
            this.source = source.Select(l => l.ToArray()).ToArray();
        }

        public IEnumerable<Blob> GetBlobs()
        {
            var startIndex = IsColumnEmpty(0) ? 1 : 0;
            var lastEmptyColumn = startIndex;

            var blobs = new List<Blob>();
            for(int x = startIndex; x <= source[0].Length; x++)
            {
                if (IsColumnEmpty(x))
                {
                    var blob = new Blob(SliceImage(lastEmptyColumn, x), lastEmptyColumn, x - 1);
                    if (!blob.IsEmpty)
                    {
                        blobs.Add(blob);
                    }
                    lastEmptyColumn = x + 1;
                }
            }

            return blobs;
        }

        private bool[][] SliceImage(int lastEmptyColumn, int currentColumn)
        {
            var rows = new List<bool[]>();
            for(int y = 0; y < source.Length; y++)
            {
                var row = new List<bool>();
                for(int x = lastEmptyColumn; x < currentColumn; x++)
                {
                    row.Add(source[y][x]);
                }
                rows.Add(row.ToArray());
            }
            return rows.ToArray();
        }

        private bool IsColumnEmpty(int x)
        {
            if (x >= source[0].Length)
            {
                return true;
            }

            for(int y = 0; y < source.Length; y++)
            {
                if (source[y][x] == true)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
