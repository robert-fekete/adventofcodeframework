// ==UserScript==
// @name         AoC leaderboard
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @include      /^https://adventofcode.com(/day/\d\d?)?$/
// @include      /^https://adventofcode.com/2\d\d\d(/day/\d\d?)?$/
// @match        https://adventofcode.com
// @icon         https://www.google.com/s2/favicons?sz=64&domain=adventofcode.com
// @grant        none
// ==/UserScript==


(function () {
    'use strict';

    function scrollTheIframe(id, offset) {
        console.log("Scrolling");
        var iframe = document.getElementById(id);
        iframe.contentWindow.scrollTo(0, offset);
    }

    var url = window.location.href;
    var match = url.match("adventofcode\.com\/([0-9]{4})");
    var year = match != null ? match[1] : new Date().getFullYear();
    var scrollOffsetForAnnouncement = 64;
    var scrollOffset = 0;
    if (year == "2018" || year == "2020") {
        scrollOffset = scrollOffsetForAnnouncement;
    }

    var sidebar = document.getElementById("sidebar");
    sidebar.innerHTML = '';
    sidebar.style.width = "700px";
    var iframe = document.createElement("iframe");
    iframe.setAttribute("src", "https://adventofcode.com/" + year + "/leaderboard/private/view/79476");
    iframe.id = "JaniBro";
    iframe.style.width = "700px";
    iframe.style.height = "138px";
    iframe.style.border = "none";
    iframe.style.marginBottom = "50px";
    iframe.scrolling = "no";
    iframe.onload = () => scrollTheIframe("JaniBro", 267 + scrollOffset);
    sidebar.appendChild(iframe);

    var iframe2 = document.createElement("iframe");
    iframe2.setAttribute("src", "https://adventofcode.com/" + year + "/leaderboard/private/view/179998");
    iframe2.id = "TomekBro";
    iframe2.style.width = "700px";
    iframe2.style.height = "138px";
    iframe2.style.border = "none";
    iframe2.scrolling = "no";
    iframe2.onload = () => scrollTheIframe("TomekBro", 244 + scrollOffset);
    sidebar.appendChild(iframe2);
})();