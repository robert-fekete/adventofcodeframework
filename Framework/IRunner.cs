﻿namespace AdventOfCode.Framework
{
    public interface IRunner
    {
        void RunAll(bool withPerformance = false);
        void RunLast(bool withPerformance = false);
        void Run(int day, bool withPerformance = false);
    }
}
