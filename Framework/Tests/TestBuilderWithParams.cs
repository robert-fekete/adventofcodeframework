﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode.Framework.Tests
{
    internal class TestBuilderWithParams<TInput, TParams, TResult> : ITestBuilder<TInput, TParams, TResult>
    {
        public readonly List<(TInput, TParams, TResult)> testCases = new List<(TInput, TParams, TResult)>();
        public Func<TInput, TParams, TResult>? testMethod;

        public ITestBuilder<TInput, TParams, TResult> AddTestCase(TInput input, TParams parameters, TResult results)
        {
            testCases.Add((input, parameters, results));

            return this;
        }

        public ITestBuilder<TInput, TParams, TResult> AddTest(Func<TInput, TParams, TResult> testMethod)
        {
            this.testMethod = testMethod;

            return this;
        }

        public ITestSuite Build()
        {
            if (testMethod == null)
            {
                throw new InvalidOperationException("Test method was not set.");
            }
            return new TestSuiteWithParams<TInput, TParams, TResult>(testCases, testMethod);
        }
    }
}
