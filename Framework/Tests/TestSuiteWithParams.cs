﻿using System;
using System.Collections.Generic;

namespace AdventOfCode.Framework.Tests
{
    internal class TestSuiteWithParams<TInput, TParams, TResult> : TestSuite
    {
        private readonly IEnumerable<(TInput, TParams, TResult)> testCases;
        private readonly Func<TInput, TParams, TResult> testMethod;

        public TestSuiteWithParams(IEnumerable<(TInput, TParams, TResult)> testCases, Func<TInput, TParams, TResult> testMethod)
        {
            this.testCases = testCases;
            this.testMethod = testMethod;
        }

        public override bool Execute()
        {
            var isSuccess = true;
            var iter = 1;
            foreach ((var input, var parameters, var expected) in testCases)
            {
                var result = testMethod(input, parameters);
                isSuccess &= EvaluateTestCase(iter, result, expected);
                iter++;
            }

            return isSuccess;
        }
    }
}
