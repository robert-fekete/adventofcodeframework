﻿using System;
using System.Collections.Generic;

namespace AdventOfCode.Framework.Tests
{
    internal class TestSuiteWithoutParams<TInput, TResult> : TestSuite
    {
        private readonly IEnumerable<(TInput, TResult)> testCases;
        private readonly Func<TInput, TResult> testMethod;

        public TestSuiteWithoutParams(IEnumerable<(TInput, TResult)> testCases, Func<TInput, TResult> testMethod)
        {
            this.testCases = testCases;
            this.testMethod = testMethod;
        }

        public override bool Execute()
        {
            var isSuccess = true;
            var iter = 1;
            foreach ((var input, var expected) in testCases)
            {
                var result = testMethod(input);
                isSuccess &= EvaluateTestCase(iter, result, expected);
                iter++;
            }

            return isSuccess;
        }
    }
}
