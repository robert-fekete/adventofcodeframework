﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode.Framework.Tests
{
    internal class TestSuite : ITestSuite
    {
        private readonly bool result;

        public TestSuite(bool result = true)
        {
            this.result = result;
        }

        public virtual bool Execute()
        {
            return result;
        }

        protected bool EvaluateTestCase<TResult>(int number, TResult actual, TResult expected)
        {
            if (EqualityComparer<TResult>.Default.Equals(actual, expected))
            {
                return true;
            }
            else
            {
                Console.WriteLine($"Test {number} failed. Expected: {expected}, Actual: {actual}");
                return false;
            }
        }
    }
}
