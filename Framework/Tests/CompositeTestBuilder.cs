﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Framework.Tests
{
    class CompositeTestBuilder : ICompositeTestBuilder
    {
        private readonly List<ITestBuilder> testBuilders = new List<ITestBuilder>();
        public ICompositeTestBuilder AddTestBuilder(Func<TestBuilder, ITestBuilder> builderFactory)
        {
            var newBuilder = new TestBuilder();
            var builder = builderFactory(newBuilder);
            testBuilders.Add(builder);

            return this;
        }

        public ITestSuite Build()
        {
            var testSuites = testBuilders.Select(b => b.Build()).ToArray();

            return new CompositeTestSuite(testSuites);
        }
    }
}
