﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode.Framework.Tests
{
    internal class TestBuilderWithoutParams<TInput, TResult> : ITestBuilder<TInput, TResult>
    {
        public readonly List<(TInput, TResult)> testCases = new List<(TInput, TResult)>();
        public Func<TInput, TResult>? testMethod;

        public ITestBuilder<TInput, TResult> AddTestCase(TInput input, TResult results)
        {
            testCases.Add((input, results));

            return this;
        }

        public ITestBuilder<TInput, TResult> AddTest(Func<TInput, TResult> testMethod)
        {
            this.testMethod = testMethod;

            return this;
        }

        public ITestSuite Build()
        {
            if (testMethod == null)
            {
                throw new InvalidOperationException("Test method was not set.");
            }
            return new TestSuiteWithoutParams<TInput, TResult>(testCases, testMethod);
        }
    }
}
