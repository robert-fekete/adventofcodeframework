﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode.Framework.Tests
{
    class CompositeTestSuite : ITestSuite
    {
        private readonly IEnumerable<ITestSuite> testSuites;

        public CompositeTestSuite(IEnumerable<ITestSuite> testSuites)
        {
            this.testSuites = testSuites;
        }

        public bool Execute()
        {
            var isSuccess = true;
            foreach (var testSuite in testSuites)
            {
                isSuccess &= testSuite.Execute();
            }

            return isSuccess;
        }
    }
}
