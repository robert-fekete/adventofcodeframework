﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace AdventOfCode.Framework
{
    public class Report
    {
        private readonly Stopwatch stopwatch = new Stopwatch();
        private readonly List<(string, double, bool)> steps = new List<(string, double, bool)>();
        private double lastSplit;

        public void Start()
        {
            steps.Clear();
            lastSplit = 0;
            stopwatch.Restart();
        }

        public void AddStep(string message, bool isSplit = false)
        {
            var ellapsed = stopwatch.Elapsed.TotalSeconds;
            if (isSplit)
            {
                steps.Add((message, ellapsed - lastSplit, isSplit));
            }
            else
            {
                steps.Add((message, ellapsed, isSplit));
            }
            lastSplit = ellapsed;
        }

        public void Stop()
        {
            stopwatch.Stop();
        }

        public void Print()
        {
            foreach((var message, var seconds, var isSplit) in steps)
            {
                var deltaToken = isSplit ? " (delta)" : string.Empty;
                Console.WriteLine($"{message}: {seconds} s{deltaToken}");
            }
            Console.WriteLine($"Total: {stopwatch.Elapsed.TotalSeconds} s");
        }
    }
}
