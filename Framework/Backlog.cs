﻿namespace AdventOfCode.Framework
{
    class Backlog
    {
        // TODO: Make a flag so days can be run silently -> doesn't print anything, unless test or main part is broken
        // TODO: Make a flag to measure run time
        // TODO: Make ISolver generic? it would be nice to return long instead of long.ToString()
        // TODO: Make an auto-parser be part of the framework? T Parse<T>(IEnumerable<string>)?
        // TODO: Option to print "Running test #1" when running tests so I have a feeling whether the tests are slow or the main is slow
    }
}
