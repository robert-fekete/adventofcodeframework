﻿using AdventOfCode.Framework.Tests;
using System.Collections.Generic;

namespace AdventOfCode.Framework
{
    public interface ISolver
    {
        int DayNumber { get; }
        string FirstExpected { get; }
        string SecondExpected { get; }
        ITestSuite GetFirstTests();
        ITestSuite GetSecondTests();

        string ExecuteFirst(IEnumerable<string> input);
        string ExecuteSecond(IEnumerable<string> input);
    }
}
