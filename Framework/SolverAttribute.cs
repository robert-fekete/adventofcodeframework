﻿using System;

namespace AdventOfCode.Framework
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class SolverAttribute : Attribute
    {
    }
}
