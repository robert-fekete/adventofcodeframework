﻿using System;

namespace AdventOfCode.Framework
{
    public interface ITestBuilder
    {
        ITestSuite Build();
    }

    public interface ICompositeTestBuilder : ITestBuilder
    {
        ICompositeTestBuilder AddTestBuilder(Func<TestBuilder, ITestBuilder> builderFactory);
    }

    public interface ITestBuilder<TInput, TParams, TResult> : ITestBuilder
    {
        ITestBuilder<TInput, TParams, TResult> AddTestCase(TInput input, TParams parameters, TResult results);
        ITestBuilder<TInput, TParams, TResult> AddTest(Func<TInput, TParams, TResult> testMethod);
    }

    public interface ITestBuilder<TInput, TResult> : ITestBuilder
    {
        ITestBuilder<TInput, TResult> AddTest(Func<TInput, TResult> testMethod);
        ITestBuilder<TInput, TResult> AddTestCase(TInput input, TResult results);
    }
}
