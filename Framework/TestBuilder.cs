﻿using AdventOfCode.Framework.Tests;
using System;

namespace AdventOfCode.Framework
{
    public class TestBuilder : ITestBuilder
    {
        public ITestBuilder<TInput, TResult> AddTestCase<TInput, TResult>(TInput input, TResult results)
        {
            var builder = new TestBuilderWithoutParams<TInput, TResult>();
            return builder.AddTestCase(input, results);
        }

        public ITestBuilder<TInput, TResult> AddTest<TInput, TResult>(Func<TInput, TResult> testMethod)
        {
            var builder = new TestBuilderWithoutParams<TInput, TResult>();
            return builder.AddTest(testMethod);
        }

        public ITestBuilder<TInput, TParams, TResult> AddTestCase<TInput, TParams, TResult>(TInput input, TParams parameters, TResult results)
        {
            var builder = new TestBuilderWithParams<TInput, TParams, TResult>();
            return builder.AddTestCase(input, parameters, results);
        }

        public ITestBuilder<TInput, TParams, TResult> AddTest<TInput, TParams, TResult>(Func<TInput, TParams, TResult> testMethod)
        {
            var builder = new TestBuilderWithParams<TInput, TParams, TResult>();
            return builder.AddTest(testMethod);
        }

        public ICompositeTestBuilder AddTestBuilder(Func<TestBuilder, ITestBuilder> builderFactory)
        {
            var builder = new CompositeTestBuilder();
            return builder.AddTestBuilder(builderFactory);
        }

        public ITestSuite Build()
        {
            return Build(true);
        }

        public ITestSuite Build(bool result)
        {
            return new TestSuite(result);
        }
    }
}
