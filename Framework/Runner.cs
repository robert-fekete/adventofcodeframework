﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode.Framework
{
    public class Runner : IRunner
    {
        private readonly IEnumerable<ISolver> solvers;

        public Runner(IEnumerable<ISolver> solvers)
        {
            this.solvers = solvers;
        }

        public void Run(int day, bool withPerformance = false)
        {
            var solver = solvers.FirstOrDefault(s => s.DayNumber == day);
            if (solver == null)
            {
                Console.WriteLine($"No solver is registered for Day {day}");
                return;
            }

            RunSolver(solver, withPerformance);
        }

        public void RunAll(bool withPerformance = false)
        {
            foreach (var solver in solvers)
            {
                RunSolver(solver, withPerformance);
            }
        }

        public void RunLast(bool withPerformance = false)
        {
            var lastDay = solvers.Max(s => s.DayNumber);
            Run(lastDay, withPerformance);
        }

        private static void RunSolver(ISolver solver, bool withPerformance)
        {
            Console.WriteLine($"Day {solver.DayNumber}:");
            Console.WriteLine("-Part 1:");

            var input = LoadInput(solver.DayNumber);
            var partOneBuilder = solver.GetFirstTests();
            if (partOneBuilder.Execute())
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                var result = solver.ExecuteFirst(input);
                stopwatch.Stop();

                if (result == solver.FirstExpected)
                {
                    Console.WriteLine($" Solution: {result}");
                    if (withPerformance)
                    {
                        Console.WriteLine($" Runtime: {stopwatch.Elapsed.TotalMilliseconds} ms");
                    }
                }
                else
                {
                    Console.WriteLine($" First part failed. Actual: {result}, Expected: {solver.FirstExpected}");
                }
            }

            Console.WriteLine("-Part 2:");
            var partTwoBuilder = solver.GetSecondTests();
            if (partTwoBuilder.Execute())
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                var result = solver.ExecuteSecond(input);
                stopwatch.Stop();

                if (result == solver.SecondExpected)
                {
                    Console.WriteLine($" Solution: {result}");
                    if (withPerformance)
                    {
                        Console.WriteLine($" Runtime: {stopwatch.Elapsed.TotalMilliseconds} ms");
                    }
                }
                else
                {
                    Console.WriteLine($" Second part failed. Actual: {result}, Expected: {solver.SecondExpected}");
                }
            }
            Console.WriteLine();
        }

        private static IEnumerable<string> LoadInput(int dayNumber)
        {
            return File.ReadAllLines($@".\Day{dayNumber}\input.txt");
        }
    }
}
