﻿namespace AdventOfCode.Framework
{
    public interface ITestSuite
    {
        bool Execute();
    }
}