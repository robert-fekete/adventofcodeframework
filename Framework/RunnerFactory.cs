﻿using System;
using System.Linq;
using System.Reflection;

namespace AdventOfCode.Framework
{
    public static class RunnerFactory
    {
        public static IRunner Create(Assembly assembly)
        {
            var solvers = assembly.GetTypes()
                                  .Where(t => t.IsDefined(typeof(SolverAttribute), false))
                                  .Select<Type, (Type Type, int Day)>(t => (t, int.Parse(t.Namespace!
                                                                                          .Split(".")
                                                                                          .Last()
                                                                                          .Substring(3))))
                                  .OrderBy(p => p.Day)
                                  .Select(p => (ISolver)Activator.CreateInstance(p.Type, p.Day)!);
            return new Runner(solvers);
        }
    }
}
